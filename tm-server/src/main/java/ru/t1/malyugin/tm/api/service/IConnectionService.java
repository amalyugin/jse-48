package ru.t1.malyugin.tm.api.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface IConnectionService {

    @NotNull
    EntityManager getEntityManager();

    void closeEntityManagerFactory();

    @NotNull Liquibase getLiquibase();

}