package ru.t1.malyugin.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.model.User;

public interface IUserService extends IService<User> {

    User create(
            @Nullable String login,
            @Nullable String pass,
            @Nullable String email,
            @Nullable Role role);

    void lockUser(@Nullable String login);

    void unlockUser(@Nullable String login);

    void removeByLogin(@Nullable String login);

    void removeByEmail(@Nullable String email);

    void setPassword(@Nullable String id, @Nullable String password);

    void update(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String middleName,
            @Nullable String lastName
    );

    @Nullable
    User findOneByEmail(@Nullable String email);

    @Nullable
    User findOneByLogin(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    boolean isLoginExist(@Nullable String login);

}