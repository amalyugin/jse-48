package ru.t1.malyugin.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.service.*;
import ru.t1.malyugin.tm.api.service.dto.IProjectDTOService;
import ru.t1.malyugin.tm.api.service.dto.ISessionDTOService;
import ru.t1.malyugin.tm.api.service.dto.ITaskDTOService;
import ru.t1.malyugin.tm.api.service.dto.IUserDTOService;
import ru.t1.malyugin.tm.api.service.model.IProjectService;
import ru.t1.malyugin.tm.api.service.model.ISessionService;
import ru.t1.malyugin.tm.api.service.model.ITaskService;
import ru.t1.malyugin.tm.api.service.model.IUserService;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;
import ru.t1.malyugin.tm.service.*;
import ru.t1.malyugin.tm.service.dto.ProjectDTOService;
import ru.t1.malyugin.tm.service.dto.SessionDTOService;
import ru.t1.malyugin.tm.service.dto.TaskDTOService;
import ru.t1.malyugin.tm.service.dto.UserDTOService;
import ru.t1.malyugin.tm.service.model.ProjectService;
import ru.t1.malyugin.tm.service.model.SessionService;
import ru.t1.malyugin.tm.service.model.TaskService;
import ru.t1.malyugin.tm.service.model.UserService;

public final class ServiceLocator implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(this);

    @Getter
    @NotNull
    private final IProjectDTOService projectDTOService = new ProjectDTOService(this);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(this);

    @Getter
    @NotNull
    private final ITaskDTOService taskDTOService = new TaskDTOService(this);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(this);

    @Getter
    @NotNull
    private final ISessionDTOService sessionDTOService = new SessionDTOService(this);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(this);

    @Getter
    @NotNull
    private final IUserDTOService userDTOService = new UserDTOService(this);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(this);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(propertyService);

    @Getter
    @NotNull
    private final ISchemeService schemeService = new SchemeService(this);

}