package ru.t1.malyugin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.AbstractDTOModel;

import java.util.Collection;
import java.util.List;

public interface IDTORepository<M extends AbstractDTOModel> {

    void add(@NotNull M model);

    void addAll(@NotNull Collection<M> modelList);

    void set(@NotNull Collection<M> modelList);

    @NotNull
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull String id);

    long getSize();

    void update(@NotNull M model);

    void remove(@NotNull M model);

    void clear();

}