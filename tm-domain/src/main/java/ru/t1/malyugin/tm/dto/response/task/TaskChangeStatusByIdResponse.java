package ru.t1.malyugin.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.malyugin.tm.dto.response.AbstractResponse;


@Getter
@Setter
@NoArgsConstructor
public class TaskChangeStatusByIdResponse extends AbstractResponse {

}