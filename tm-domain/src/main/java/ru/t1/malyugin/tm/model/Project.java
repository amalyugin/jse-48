package ru.t1.malyugin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public final class Project extends AbstractWBSModel {

    private static final long serialVersionUID = 1;

    @Column
    @NotNull
    private String description = "";

    @NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> taskList = new ArrayList<>();

    public Project(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    @NotNull
    public String toString() {
        @NotNull String result = "";
        boolean isDescription = StringUtils.isBlank(description);

        result += String.format("%s: %s IN STATUS '%s'", id, name, status.getDisplayName());
        result += (isDescription ? " -> " + description : "");
        return result;
    }

}